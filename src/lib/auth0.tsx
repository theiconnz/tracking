import {initAuth0} from "@auth0/nextjs-auth0";
import {SignInWithAuth0} from '@auth0/nextjs-auth0/dist/instance'

const auth0: SignInWithAuth0 = initAuth0({

})

export default auth0;

