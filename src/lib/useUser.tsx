import {useState, useEffect} from "react";
import {UserProfile} from '@auth0/nextjs-auth0'

export async function fetchUser(cookie:any = ''){
    if (typeof window !== 'undefined' && (typeof window.__user!=='undefined')) {
        return window.__user;
    }

    const res = await fetch(
        'api/user',
        cookie ? {
            headers : {
                cookie,
            }
        } : {}
    )


}
