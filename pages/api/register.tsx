// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'

type Data = {
  status: string,
  message: string
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  let error = true;
  let message = 'Invalide data';
  let data : object = {
    status:  error,
    message: 'No access right'
  }

  if (req.method === 'POST') {
    error = false;
  }

  if(!error){
    res.status(200).json({status: 'success', message: 'Success'})
    return;
  }

  res.status(400).json({status: 'error', message: message});
}
