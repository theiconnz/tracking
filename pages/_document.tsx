import { Html, Head, Main, NextScript } from 'next/document'
import React from "react";
import {useRouter} from "next/router";
import LoadingScreen from '../src/components/loadingScreen'

export default function Document() {
    const[pageLoading, setPageLoading] = React.useState<boolean>(false);
    const router = useRouter();

    React.useEffect(() =>{
        let handleStart = () => {setPageLoading(true); console.log('start');}
        let handleComplete = () => {setPageLoading(false); console.log('finished');}

        router.events.on('routeChangeStart', handleStart )
        router.events.on('routeChangeComplete', handleComplete);
        router.events.on('routeChangeError', handleComplete);

        document.title = "";
    }, [router]);


    return (
        <Html className="dark">
            <Head />
            <body className="bg-white dark:bg-slate-800">
            <Main />
            <NextScript />
            </body>
        </Html>
    )
}