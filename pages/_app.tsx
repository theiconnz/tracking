import React from "react";
import type { AppProps } from 'next/app'
import LoadingScreen from '../src/components/loadingScreen'
import '../styles/globals.css'
import { useRouter } from 'next/router';

function TrackingApp({ Component, pageProps }:AppProps) {
    const[pageLoading, setPageLoading] = React.useState<boolean>(false);
    const router = useRouter();

    React.useEffect(() =>{
        let handleStart = () => {setPageLoading(true);}
        let handleComplete = () => {setPageLoading(false);}

        router.events.on('routeChangeStart', handleStart )
        router.events.on('routeChangeComplete', handleComplete);
        router.events.on('routeChangeError', handleComplete);
    }, [router]);


    return (
        <>
            {!pageLoading?(
                <React.Fragment>
                    <Component {...pageProps} />
                </React.Fragment>
            ):(
                <LoadingScreen />
            )}
        </>
    )
}

export default TrackingApp